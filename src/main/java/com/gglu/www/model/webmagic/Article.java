package com.gglu.www.model.webmagic;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2018/1/28
 * @description
 */
@Data
@TableName("article")
public class Article implements Serializable{

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String content;

    private String summary;

    private String title;

    private String url;

    private String star;

    private String praise;

    private String scan;

    private Date writeDate;

    private Date createDate;
}
