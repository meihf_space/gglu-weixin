package com.gglu.www.util.aes;

import com.gglu.www.wechatsecurity.WeChatSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2018/2/4
 * @description
 */
public class SignUtil {
    /**
     * 与开发模式接口配置信息中的Token保持一致
     */
    private static String token = "feng521Feng";

    /**
     * 微信生成的 ASEKey
     */
    private static String encodingAesKey = "V7DsPl9hpHhuvKe3cohxHlZ6w3Xh422bHkoAmHjJEZt";

    /**
     * 应用的AppId
     */
    private static String appId = "wxc5ee63019fad1990";

    private Logger logger = LoggerFactory.getLogger(SignUtil.class);
    /**
     * 解密微信发过来的密文
     *
     * @return 加密后的内容
     */
    public static String decryptMsg(String msgSignature, String timeStamp, String nonce, String encrypt_msg) {
        WXBizMsgCrypt pc;
        String result = "";
        try {
            pc = new WXBizMsgCrypt(token, encodingAesKey, appId);
            result = pc.decryptMsg(msgSignature, timeStamp, nonce, encrypt_msg);
        } catch (AesException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 加密给微信的消息内容
     *
     * @param replayMsg
     * @param timeStamp
     * @param nonce
     * @return
     */
    public static String ecryptMsg(String replayMsg, String timeStamp, String nonce) {
        WXBizMsgCrypt pc;
        String result = "";
        try {
            pc = new WXBizMsgCrypt(token, encodingAesKey, appId);
            result = pc.encryptMsg(replayMsg, timeStamp, nonce);
        } catch (AesException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
}
