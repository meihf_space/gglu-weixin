package com.gglu.www.util;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2017/7/21
 * @description
 */
public class GlobalErr {

    public static final Integer C10000 = 10000;
    public static final String E10000 = "用户不存在";

    public static final Integer C10001 = 10001;
    public static final String E10001 = "用户名或密码错误";

    public static final Integer C10002 = 10003;
    public static final String E10002 = "登录成功";

    public static final Integer C10004 = 10004;
    public static final String E10004 = "编辑失败";

    public static final Integer C10005 = 10005;
    public static final String E10005 = "编辑失败";

    public static final Integer C10006 = 10006;
    public static final String E10006 = "授权失败";

    public static final Integer C10007 = 10007;
    public static final String E10007 = "获取用户信息失败";

    public static final Integer C10008 = 10008;
    public static final String E10008 = "新增失败";

    public static final Integer C10009 = 10009;
    public static final String E10009 = "删除失败";

    public static final Integer C10010 = 10010;
    public static final String E10010 = "已发货状态，不能修改";

    public static final Integer C10011 = 10011;
    public static final String E10011 = "查询结果为空";

    public static final Integer C10012 = 10012;
    public static final String E10012 = "网络错误";

    public static final Integer C10013 = 10013;
    public static final String E10013 = "截止时间不能小于开始时间";

    public static final Integer C10014 = 10014;
    public static final String E10014 = "库中已存在此类型的优惠券";

    public static final Integer C10015 = 10015;
    public static final String E10015 = "网络错误";

    public static final Integer C10016 = 10016;
    public static final String E10016 = "网络错误";


    public static final Integer C10017 = 10017;
    public static final String E10017 = "优惠码不存在";

    public static final Integer C10018 = 10018;
    public static final String E10018 = "优惠码已过期";

    public static final Integer C10019 = 10019;
    public static final String E10019 = "优惠码不可用";

    public static final Integer C10020 = 10020;
    public static final String E10020 = "网络错误";

    public static final Integer C10021 = 10021;
    public static final String E10021 = "上传失败";




}
