package com.gglu.www.util;

import java.util.Random;

/**
 * @author Loysen
 * @company 杭州新苗网络科技有限公司
 * @date 2017/6/29  19:54
 * @
 */
public class RandomStringUtils {

    // 1、生成的字符串每个位置都有可能是str中的一个字母或数字，需要导入的包是import java.util.Random;
    //length用户要求产生字符串的长度
    public static String getRandomString(int length){
        String str="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number = random.nextInt(36);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    //2、可以指定某个位置是a-z、A-Z或是0-9，需要导入的包是import java.util.Random;
    //可以指定字符串的某个位置是什么范围的值
    public static String getRandomString2(int length){
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(3);
            long result=0;
            switch(number){
                case 0:
                    result=Math.round(Math.random()*25+65);
                    sb.append(String.valueOf((char)result));
                    break;
                case 1:
                    result=Math.round(Math.random()*25+97);
                    sb.append(String.valueOf((char)result));
                    break;
                case 2:
                    sb.append(String.valueOf(new Random().nextInt(10)));
                    break;
            }

        }
        return sb.toString();
    }
    //3、org.apache.commons.lang包下有一个RandomStringUtils类，其中有一个randomAlphanumeric(int length)函数，可以随机生成一个长度为length的字符串。
    // String filename=RandomStringUtils.randomAlphanumeric(10);

    public static void main(String[] args) {
       System.out.println(RandomStringUtils.getRandomString(8));
       System.out.println(RandomStringUtils.getRandomString(8));
       System.out.println(RandomStringUtils.getRandomString(8));
       System.out.println(RandomStringUtils.getRandomString(8));
    /*    Map  map = new HashMap();
       for (int i= 0;i<100000000 ; i++ ){
           map.put(RandomStringUtils.getRandomString(8),1 );
          // System.out.println(RandomStringUtils.getRandomString(8));
       }
*/
       // System.out.println(map.size());
        //System.out.println("abcdefghijklmnopqrstuvwxyz".length());
    }

}
