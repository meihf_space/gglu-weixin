package com.gglu.www.dto.wechat.send;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2018/2/4
 * @description
 */
public class VideoMessage {

    private Video Video;

    public Video getVideo() {
        return Video;
    }

    public void setVideo(Video video) {
        Video = video;
    }

}
