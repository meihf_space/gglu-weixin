package com.gglu.www.service.webmagic;

import com.baomidou.mybatisplus.service.IService;
import com.gglu.www.model.webmagic.Article;

/**
 * 杭州东导数据技术有限公司
 *
 * @author meihf
 * @create 2017/10/31
 * @description
 */
public interface ArticleService extends IService<Article> {
}
