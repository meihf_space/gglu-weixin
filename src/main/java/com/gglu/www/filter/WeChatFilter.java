package com.gglu.www.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @desc:处理支付的访问路径
 * @author 梅海风
 */
public class WeChatFilter extends OncePerRequestFilter {

	private static Log LOGGER = LogFactory.getLog(WeChatFilter.class);
	
	@Override
	protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if (!request.getServletPath().equals("/static/tea/details/") && !request.getServletPath().equals("/static/tea/cart/")) {
			filterChain.doFilter(request, response);
		} else {
			LOGGER.info("处理支付的访问路径");
			request.getRequestDispatcher(request.getServletPath()+"index.html").forward(request,response);
		}
	}


}