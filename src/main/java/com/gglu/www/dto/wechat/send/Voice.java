package com.gglu.www.dto.wechat.send;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2018/2/4
 * @description
 */
public class Voice {

    private String MediaId;

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }
}
