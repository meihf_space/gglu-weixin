package com.gglu.www.service.impl.webmagic;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.gglu.www.mapper.webmagic.ArticleMapper;
import com.gglu.www.model.webmagic.Article;
import com.gglu.www.service.webmagic.ArticleService;
import org.springframework.stereotype.Service;

/**
 * 杭州东导数据技术有限公司
 *
 * @author meihf
 * @create 2017/10/31
 * @description
 */
@Service("articleService")
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper,Article> implements ArticleService {
}
