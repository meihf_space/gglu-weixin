package com.gglu.www.dto;

import lombok.Data;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2017/8/27
 * @description
 */
@Data
public class ReturnUploadDto {

    private String state;//上传状态SUCCESS 一定要大写
    private String url;//上传地址
    private String title;//图片名称demo.jpg
    private String original;//图片名称demo.jpg
}
