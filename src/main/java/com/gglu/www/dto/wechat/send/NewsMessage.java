package com.gglu.www.dto.wechat.send;

import lombok.Data;

import java.util.List;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2018/2/4
 * @description
 */
@Data
public class NewsMessage extends BaseMessage {

    // 图文消息个数，限制为 10 条以内
    private int articleCount;
    // 多条图文消息信息，默认第一个 item 为大图
    private List<Article> articles;


}
