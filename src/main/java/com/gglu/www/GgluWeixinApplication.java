package com.gglu.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GgluWeixinApplication extends ApplicationInitialier{

	public static void main(String[] args) {
		SpringApplication.run(GgluWeixinApplication.class, args);
		System.out.println("start");
	}
}
