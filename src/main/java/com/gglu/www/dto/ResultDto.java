package com.gglu.www.dto;


import com.gglu.www.util.DateUtils;

/**
 * 杭州新苗网络科技有限公司
 *
 * @author meihf
 * @create 2017/7/24
 * @description
 */
public class ResultDto {

    private static final long serialVersionUID = 5576237395711742681L;
    private Integer code = Integer.valueOf(0);
    private String msg = "";
    private Long time = DateUtils.getTimeSecondLong();
    private Object data = null;

    public ResultDto() {
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getTime() {
        return this.time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
