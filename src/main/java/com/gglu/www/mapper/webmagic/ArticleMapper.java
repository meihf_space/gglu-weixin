package com.gglu.www.mapper.webmagic;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.gglu.www.model.webmagic.Article;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 杭州东导数据技术有限公司
 *
 * @author meihf
 * @create 2017/10/31
 * @description
 */
public interface ArticleMapper extends BaseMapper<Article> {
}
